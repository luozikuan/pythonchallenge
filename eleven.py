# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/return/5808.html

import urllib, os, string
import Image

if not os.path.exists('cave.jpg'):
    source = urllib.urlopen('http://huge:file@www.pythonchallenge.com/pc/return/cave.jpg').read()
    with open('cave.jpg', 'w') as f:
        f.write(source)

im = Image.open('cave.jpg')

ia = Image.new('RGB', im.size, (0, 0, 0))
ib = Image.new('RGB', im.size, (0, 0, 0))
for x in xrange(im.size[0]):
    for y in xrange(im.size[1]):
        if (x+y) % 2 == 0:
            ia.putpixel((x, y), im.getpixel((x, y)))
        else:
            ib.putpixel((x, y), im.getpixel((x, y)))
ia.show()
ib.show()

# next level: http://www.pythonchallenge.com/pc/return/evil.html
