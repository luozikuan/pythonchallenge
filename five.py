# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/peak.html

import urllib, pickle

try:
    with open('banner.p', 'r') as f:
    banner = f.read()
except IOError:
    banner = urllib.urlopen('http://www.pythonchallenge.com/pc/def/banner.p').read()
    with open('banner.p', 'w') as f:
        f.write(banner)

# print banner
m = pickle.loads(banner)
for line in m:
    print ''.join(map(lambda x: x[0] * x[1], line))

# next level: http://www.pythonchallenge.com/pc/def/channel.html
