# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/linkedlist.html
# then
# http://www.pythonchallenge.com/pc/def/linkedlist.php

import urllib, re

num = '12345'
num = '8022' # error when num is 16044
num = '66831' # answer
for i in xrange(40):
    print num
    result = urllib.urlopen('http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=%s' % num).read()
    numlist = re.findall(r'and the next nothing is (\d+)', result)

    if len(numlist) != 0:
        num = numlist[0]
    else:
        print result
        break

# next level: http://www.pythonchallenge.com/pc/def/peak.html