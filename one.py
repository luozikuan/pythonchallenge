# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/map.html

import string
def lettersAddTwo(src):
    transformed = string.ascii_lowercase[2:] + string.ascii_lowercase[:2]
    table = string.maketrans(string.ascii_lowercase, transformed)
    return src.translate(table)

src = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
print(lettersAddTwo(src))

print(lettersAddTwo('map'))

# next level:
# http://www.pythonchallenge.com/pc/def/ocr.html
