# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/oxygen.html

import urllib, os, string
import Image

if not os.path.exists('oxygen.png'):
    source = urllib.urlopen('http://www.pythonchallenge.com/pc/def/oxygen.png').read()
    with open('oxygen.png', 'w') as f:
        f.write(source)

im = Image.open('oxygen.png')
height = 0;

for y in xrange(im.size[1] - 1):
    if im.getpixel((0, y)) == im.getpixel((0, y+1)):
        height = y
        break
    pass

height = 45 # result get from last for loop

msg = []

for x in xrange(0, im.size[0] - 1, 7):
    pixel = im.getpixel((x, height))
    if (pixel[0] == pixel[1] == pixel[2]):
        msg.append(pixel[0])
        pass
    pass

print ''.join([chr(c) for c in msg])
msg = [105, 110, 116, 101, 103, 114, 105, 116, 121]
print ''.join([chr(c) for c in msg])

# next level: http://www.pythonchallenge.com/pc/def/integrity.html
