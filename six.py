# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/channel.html

import urllib, re, zipfile

try:
    with open('channel.zip', 'r') as f:
        channel = f.read()
except IOError:
    channel = urllib.urlopen('http://www.pythonchallenge.com/pc/def/channel.zip').read()
    with open('channel.zip', 'w') as f:
        f.write(channel)

f = zipfile.ZipFile('channel.zip', 'r')
num = '90052'
comment = []
# num = '46145' # answer: collect the comments
for i in xrange(1000):
    result = f.open('%s.txt' % num, 'r').read()
    comment.append(f.getinfo('%s.txt' % num).comment)
    numlist = re.findall(r'Next nothing is (\d+)', result)
    if len(numlist) != 0:
        num = numlist[0]
    else:
        print result
        break

print ''.join(comment)

# note: hockey is not the correct answer
# next level: http://www.pythonchallenge.com/pc/def/oxygen.html
