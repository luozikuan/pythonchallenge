# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/return/bull.html

a = [1]

for i in range(30):
    b = []
    num, cnt = -1, 0
    for x in a:
        if num == -1 and cnt == 0:
            num, cnt = x, 1
        else:
            if x == num:
                cnt = cnt + 1
            else:
                b.append(cnt)
                b.append(num)
                num, cnt = x, 1
                pass
            pass
        pass
    b.append(cnt)
    b.append(num)
    #print b
    print "len(a[%d]) = %d" % (i+1, len(b))
    a = b

# next level: http://www.pythonchallenge.com/pc/return/5808.html
