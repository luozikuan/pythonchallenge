# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/equality.html

import urllib, re

source = urllib.urlopen('http://www.pythonchallenge.com/pc/def/equality.html').read()
#source = open('3.html', 'r').read()

start = source.find("<!--") + 4
end = source.find("-->", start)

txt = source[start:end]

# print txt
print ''.join(re.findall(r'[^A-Z][A-Z]{3}([a-z])[A-Z]{3}[^A-Z]', txt))

# next level: http://www.pythonchallenge.com/pc/def/linkedlist.html
#
