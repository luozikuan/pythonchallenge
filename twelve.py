# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/return/evil.html

import urllib, os, string

filename = 'evil2.gfx'
if not os.path.exists(filename):
    source = urllib.urlopen('http://huge:file@www.pythonchallenge.com/pc/return/%s' % filename).read()
    with open(filename, 'w') as f:
        f.write(source)
        pass
    pass

with open(filename) as f:
    data = f.read()

littleFile = ['1', '2', '3', '4', '5']
cur = 0
for d in data:
    with open(littleFile[cur], 'a') as f:
        f.write(d)
        pass
    cur = (cur + 1) % 5
    pass

# next level: http://www.pythonchallenge.com/pc/return/disproportional.html
