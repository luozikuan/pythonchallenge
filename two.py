# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/ocr.html

import string, urllib

source = urllib.urlopen('http://www.pythonchallenge.com/pc/def/ocr.html').read()

start = source.find("<!--")
end = source.find("-->", start)

start = source.find("<!--", end) + 4
end = source.find("-->", start)

txt = source[start:end]

# print txt
print ''.join([c for c in txt if c in string.lowercase])

# next level: http://www.pythonchallenge.com/pc/def/equality.html
#
