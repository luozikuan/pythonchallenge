# -*- coding: utf-8 -*-
#!/usr/bin/python

# this level: http://www.pythonchallenge.com/pc/def/0.html

print(2**38)

# next level:
# http://www.pythonchallenge.com/pc/def/274877906944.html
# or
# http://www.pythonchallenge.com/pc/def/map.html